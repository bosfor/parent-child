import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { ParentComponent } from './components/parent/parent.component';

const routes: Routes = [
  { path: 'home', component: ParentComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
