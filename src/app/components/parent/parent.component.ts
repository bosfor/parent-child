import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  message: string = 'This string is in Parrent component TS file!';
  id: any;
  name:any;
  age:any;
  items = ['item1', 'item2', 'item3', 'item4'];
  form: FormGroup;
  phoneNum:any;

  constructor(fb:FormBuilder) {
    this.form=fb.group({
      phone:['']
    })
  }

  ngOnInit(): void {
  }

  onSubmit(form: any){
    console.log(form);
  }

  parentFunction(data: any) {
    console.warn(data);
    this.id = data.id;
    this.name = data.name;
    this.age =data.age;
  }

  addItem(newItem: string) {
    console.log(newItem)
    this.items.push(newItem);
  }
  keyPress(event: any){
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
