import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Output() parentFunction :EventEmitter<any> =  new EventEmitter();
  // @Output() newItemEvent = new EventEmitter<string>();
  @Input() data: string | undefined;

  constructor() { }

  ngOnInit(): void {
    
  }

  send() {
    let user = { id: 1, name: 'Borko Stankovic', age: 36};
    this.parentFunction.emit(user);
  }

  addNewItem(value: string) {
    this.parentFunction.emit(value);
  }

}
